# Docker Image -  Laravel + Vue

php:8.4
mysql:latest
node:22.9.0
## Run your containers:

docker compose build --no-cache php-fpm nginx mysql redis workspace node


## Run:

1 - Start your containers:

 docker compose up -d workspace;

2 - Start workspace (nginx mysql redis):

docker compose exec workspace bash

3 - Start node container:

docker compose exec node bash